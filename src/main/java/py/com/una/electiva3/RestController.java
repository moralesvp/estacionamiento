package py.com.una.electiva3;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;

import py.com.una.electiva3.domain.Registro;
import py.com.una.electiva3.domain.Usuario;
import py.com.una.electiva3.repository.RegistroRepository;
import py.com.una.electiva3.repository.UsuarioRepository;

@Controller
@RequestMapping("/estacionamiento/rest")
public class RestController {
	
	@Inject
	RegistroRepository registroRepository;
	
	@Inject
	UsuarioRepository usuarioRepository;
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
    public @ResponseBody JsonResult login(@RequestParam(value="username") String username, @RequestParam(value="password") String password) {
		
		if(usuarioRepository.findByUsername("admin")==null){
			Usuario user = new Usuario();
			user.setUsername("admin");
			user.setPassword("8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92");
			usuarioRepository.save(user);
		}
		
		Usuario usuario = usuarioRepository.findByUsername(username);
		if(usuario == null) {
			JsonResult result = new JsonResult(false, "No existe el usuario " + username + "! =(");
			return result;
		}
		
		String expectedPassword = hasPassword(password);
		if(!usuario.getPassword().equals(expectedPassword)) {
			JsonResult result = new JsonResult(false, "Password invalido, usuario " + username + "! =(");
			return result;
		}
		
		JsonResult result = new JsonResult(true, "Login exitoso! =)");		
		return result;
    }
	
	/*
	 * Example
	 * Password: 123456
	 * Digest: 8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92
	 */
	private String hasPassword(String password) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");			
			md.update(password.getBytes());	        
			byte byteData[] = md.digest();	 

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}
	
	@RequestMapping(value="/logout", method=RequestMethod.POST)
    public @ResponseBody JsonResult logout(@RequestParam(value="username") String username) {
		JsonResult result = new JsonResult(true, "Logout exitoso! =)");		
		return result;
    }
	
	@RequestMapping(value="/registrar/entrada", method=RequestMethod.POST)
	public @ResponseBody JsonResult resitrarEntrada(@RequestParam(value="fechaIngreso") @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm") Date fechaIngreso,
    		@RequestParam(value="nroChapa") String nroChapa,
    		@RequestParam(value="ubicacionLlave") String ubicacionLlave) {
		
		if(nroChapa.isEmpty() || ubicacionLlave.isEmpty()) {
			JsonResult result = new JsonResult(false, "Campos de registro incompletos! =(");		
			return result;
		}

		Registro registro = new Registro();
		registro.setFechaIngreso(fechaIngreso);
		registro.setNroChapa(nroChapa);
		registro.setUbicacionLlave(ubicacionLlave);
		registroRepository.save(registro);
		
		JsonResult result = new JsonResult(true, "Registro de entrada exitoso! =)", registro);		
		return result;
    }
	
	@RequestMapping(value="/registrar/salida", method=RequestMethod.POST)
    public @ResponseBody JsonResult resitrarSalida(@RequestParam(value="nroChapa") String nroChapa,
    		@RequestParam(value="fechaSalida") Date fechaSalida,
    		@RequestParam(value="horas") int horas,
    		@RequestParam(value="monto") int monto) {
        
		Registro registro = registroRepository.findByNroChapa(nroChapa);
		if(registro == null) {
			JsonResult result = new JsonResult(false, "No se encuentra registrado el nroChapa " + nroChapa +" =(");		
			return result;
		}
		registro.setFechaSalida(fechaSalida);
		registro.setHoras(horas);
		registro.setMonto(monto);
		registroRepository.save(registro);
		
		JsonResult result = new JsonResult(true, "Registro de salida exitoso! =)", registro);		
		return result;
    }
	
	@RequestMapping(value="/buscar/entrada", method=RequestMethod.POST)
	public @ResponseBody JsonResult buscarEntrada(@RequestParam(value="nroChapa") String nroChapa) {		
		
		if(nroChapa.isEmpty()) {
			JsonResult result = new JsonResult(false, "nroChapa inválido! =(");		
			return result;
		}
		
		Registro registro = registroRepository.findByNroChapa(nroChapa);
		if(registro == null) {
			JsonResult result = new JsonResult(false, "No se encuentra registrado el nroChapa " + nroChapa +" =(");		
			return result;
		}

		JsonResult result = new JsonResult(true, "Registro de entrada encontrado! =)", registro);		
		return result;
    }
	
	@RequestMapping(value="/buscar/entradas", method=RequestMethod.POST)
	public @ResponseBody JsonResult buscarEntradas() {		
		
		List<Registro> registro = registroRepository.findEntradas();
		if(registro == null) {
			JsonResult result = new JsonResult(false, "No existen entradas =(");		
			return result;
		}

		JsonResult result = new JsonResult(true, "Entradas encontradas! =)", registro);		
		return result;
    }
	
	@RequestMapping(value="/buscar/salidas", method=RequestMethod.POST)
	public @ResponseBody JsonResult buscarSalidas() {		
		
		List<Registro> registro = registroRepository.findSalidas();
		if(registro == null) {
			JsonResult result = new JsonResult(false, "No existen salidas =(");		
			return result;
		}

		JsonResult result = new JsonResult(true, "Salidas encontradas! =)", registro);		
		return result;
    }
	
}
