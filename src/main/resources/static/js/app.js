var estacionamientoApp = angular.module('estacionamientoApp', ['ui.router']);

estacionamientoApp.config(function($stateProvider, $urlRouterProvider) {

	$stateProvider
	
	.state('login', {
		url: '/login',
		templateUrl: 'templates/login.html',
		controller: 'LoginCtrl'
	})

	.state('estacionamiento', {
		url: '/estacionamiento',
		templateUrl: 'templates/menu.html'
	})	

	.state('estacionamiento.registrar-entrada', {
		url: '/registrar-entrada',
		templateUrl: 'templates/registrar-entrada.html',
		controller: 'RegistrarEntradaCtrl'
	})

	.state('estacionamiento.registrar-salida', {
		url: '/registrar-salida',
		templateUrl: 'templates/registrar-salida.html',
		controller: 'RegistrarSalidaCtrl'
	});
	
	$urlRouterProvider.otherwise('/login');

});