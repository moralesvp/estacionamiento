package py.com.una.electiva3.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Registro implements Serializable {

	private static final long serialVersionUID = -8332140747644153075L;

	@Id
	@GeneratedValue
	private int id;
	
	@Column(nullable = false)
	private Date fechaIngreso;
	
	private Date fechaSalida;
	
	@Column(nullable = false)
	private String nroChapa;
	
	@Column(nullable = false)
	private String ubicacionLlave;
	
	@Column(nullable = false)
	private int horas;
	
	@Column(nullable = false)
	private int monto;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public Date getFechaSalida() {
		return fechaSalida;
	}
	public void setFechaSalida(Date fechaSalida) {
		this.fechaSalida = fechaSalida;
	}
	public String getNroChapa() {
		return nroChapa;
	}
	public void setNroChapa(String nroChapa) {
		this.nroChapa = nroChapa;
	}
	public String getUbicacionLlave() {
		return ubicacionLlave;
	}
	public void setUbicacionLlave(String ubicacionLlave) {
		this.ubicacionLlave = ubicacionLlave;
	}
	public int getHoras() {
		return horas;
	}
	public void setHoras(int horas) {
		this.horas = horas;
	}
	public int getMonto() {
		return monto;
	}
	public void setMonto(int monto) {
		this.monto = monto;
	}
}
