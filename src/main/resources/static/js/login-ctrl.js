'use strict';

angular.module('estacionamientoApp').controller('LoginCtrl',

	function LoginCtrl($scope, $state, estacionamientoApi) {

		$scope.user = {};

		$scope.login = function() {
			if($scope.user.username && $scope.user.password){
				estacionamientoApi.login($scope.user.username, $scope.user.password).then(function(data) {
					if(data.successful) {
						$state.go("estacionamiento.registrar-entrada");
					} else {
						//TODO Mostrar mensaje de error, usar data.message
					}					
				});	
				
			}
		};

	}
);