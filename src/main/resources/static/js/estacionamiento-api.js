'use strict';

angular.module('estacionamientoApp').factory('estacionamientoApi', function($http, $q, $filter) {

	var BASE_URI = "https://localhost:8443/estacionamiento/rest";
	var LOGIN_URI = BASE_URI + "/login";
	var LOGOUT_URI = BASE_URI + "/logout";
	var REGISTRAR_ENTRADA_URI = BASE_URI + "/registrar/entrada";
	var REGISTRAR_SALIDA_URI = BASE_URI + "/registrar/salida";
	var BUSCAR_ENTRADA_URI = BASE_URI + "/buscar/entrada";
	var BUSCAR_ENTRADAS_URI = BASE_URI + "/buscar/entradas";
	var BUSCAR_SALIDAS_URI = BASE_URI + "/buscar/salidas";


	function login(username, password) {

		var deferred = $q.defer();
		var params = 'username='+username+'&password='+password;

		$http.post(LOGIN_URI, params, {headers: { 'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}})
			.success(function(data, status) {
				console.log("Received shedule data via HTTP.", data, status);
				deferred.resolve(data);
			})
			.error(function(data) {
				console.log("Error while making HTTP call.");
				deferred.reject();
			});

		return deferred.promise;
	}
	
	function registrarEntrada(fechaIngreso, nroChapa, ubicacionLlave) {

		var deferred = $q.defer();
		var params = 'fechaIngreso='+fechaIngreso+'&nroChapa='+nroChapa+'&ubicacionLlave='+ubicacionLlave;

		$http.post(REGISTRAR_ENTRADA_URI, params, {headers: { 'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}})
			.success(function(data, status) {
				console.log("Received shedule data via HTTP.", data, status);
				deferred.resolve(data);
			})
			.error(function(data) {
				console.log("Error while making HTTP call.");
				deferred.reject();
			});

		return deferred.promise;
	}
	
	function buscarEntrada(nroChapa) {

		var deferred = $q.defer();
		var params = 'nroChapa='+nroChapa;

		$http.post(BUSCAR_ENTRADA_URI, params, {headers: { 'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}})
			.success(function(data, status) {
				console.log("Received shedule data via HTTP.", data, status);
				deferred.resolve(data);
			})
			.error(function(data) {
				console.log("Error while making HTTP call.");
				deferred.reject();
			});

		return deferred.promise;
	}
	
	function buscarEntradas() {

		var deferred = $q.defer();

		$http.post(BUSCAR_ENTRADAS_URI, null, {headers: { 'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}})
			.success(function(data, status) {
				console.log("Received shedule data via HTTP.", data, status);
				deferred.resolve(data);
			})
			.error(function(data) {
				console.log("Error while making HTTP call.");
				deferred.reject();
			});

		return deferred.promise;
	}
	
	function buscarSalidas() {

		var deferred = $q.defer();

		$http.post(BUSCAR_SALIDAS_URI, null, {headers: { 'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}})
			.success(function(data, status) {
				console.log("Received shedule data via HTTP.", data, status);
				deferred.resolve(data);
			})
			.error(function(data) {
				console.log("Error while making HTTP call.");
				deferred.reject();
			});

		return deferred.promise;
	}
	
	function registrarSalida(nroChapa, fechaSalida, horas, monto) {

		var deferred = $q.defer();
		var params = 'nroChapa='+nroChapa+'&fechaSalida='+fechaSalida+'&horas='+horas+'&monto='+monto;

		$http.post(REGISTRAR_SALIDA_URI, params, {headers: { 'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}})
			.success(function(data, status) {
				console.log("Received shedule data via HTTP.", data, status);
				deferred.resolve(data);
			})
			.error(function(data) {
				console.log("Error while making HTTP call.");
				deferred.reject();
			});

		return deferred.promise;
	}
	
	return {

		login: login,
		
		registrarEntrada: registrarEntrada,
		
		buscarEntrada: buscarEntrada,
		
		buscarEntradas: buscarEntradas,
		
		buscarSalidas: buscarSalidas,
		
		registrarSalida: registrarSalida,
	}
});