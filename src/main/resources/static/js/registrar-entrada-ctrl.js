'use strict';

angular.module('estacionamientoApp').controller('RegistrarEntradaCtrl',

	function RegistrarEntradaCtrl($scope, $state, $filter, estacionamientoApi) {

		$scope.registro = {};
		$scope.registro.fechaIngreso = $filter('date')(new Date(), 'dd/MM/yyyy HH:mm');
		
		estacionamientoApi.buscarEntradas().then(function(data) {
			if(data.successful) {
				$scope.registro.entradas = data.result;
			}		
		});

		$scope.registrarEntrada = function() {
			if($scope.registro.nroChapa && $scope.registro.ubicacionLlave){
				estacionamientoApi.registrarEntrada($scope.registro.fechaIngreso, $scope.registro.nroChapa, $scope.registro.ubicacionLlave).then(function(data) {
					if(data.successful) {

						estacionamientoApi.buscarEntradas().then(function(data) {
							if(data.successful) {
								$scope.registro.entradas = data.result;
								$scope.registro.fechaIngreso = $filter('date')(new Date(), 'dd/MM/yyyy HH:mm');
								$scope.registro.nroChapa = "";
								$scope.registro.ubicacionLlave = "";
							}		
						});
						
					}		
				});	
				
			}
		};

	}
);