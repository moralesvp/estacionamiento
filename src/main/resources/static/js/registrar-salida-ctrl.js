'use strict';

angular.module('estacionamientoApp').controller('RegistrarSalidaCtrl',

	function RegistrarSalidaCtrl($scope, $state, $filter, estacionamientoApi) {

		var monto = 5000;
		$scope.registro = {};
		estacionamientoApi.buscarSalidas().then(function(data) {
			if(data.successful) {
				$scope.registro.salidas = data.result;
			}		
		});

		$scope.buscarEntrada = function() {
			if($scope.registro.nroChapa){
				estacionamientoApi.buscarEntrada($scope.registro.nroChapa).then(function(data) {
					if(data.successful) {
						var fechaIngreso = data.result.fechaIngreso;
						var fechaSalida = new Date();

						$scope.registro.fechaIngreso = $filter('date')(fechaIngreso, 'dd/MM/yyyy HH:mm');						
						$scope.registro.fechaSalida = $filter('date')(fechaSalida, 'dd/MM/yyyy HH:mm');
						$scope.registro.ubicacionLlave = data.result.ubicacionLlave;
						$scope.registro.horas = Math.ceil( (fechaSalida - fechaIngreso) / 36e5);
						$scope.registro.monto = $scope.registro.horas * monto;
						
					} else {
						//TODO Mostrar mensaje de error, usar data.message
					}		
				});	
				
			}
		};
		
		
		$scope.registrarSalida = function() {
			if($scope.registro.nroChapa && $scope.registro.ubicacionLlave && $scope.registro.horas && $scope.registro.monto){
				estacionamientoApi.registrarSalida($scope.registro.nroChapa, $scope.registro.fechaSalida, $scope.registro.horas, $scope.registro.monto).then(function(data) {
					if(data.successful) {
						estacionamientoApi.buscarSalidas().then(function(data) {
							if(data.successful) {
								$scope.registro.salidas = data.result;
								$scope.registro.fechaIngreso = "";
								$scope.registro.fechaSalida = "";
								$scope.registro.nroChapa = "";
								$scope.registro.ubicacionLlave = "";
								$scope.registro.horas = "";
								$scope.registro.monto = "";
							}		
						});
					}		
				});	
				
			}
		};

	}
);