package py.com.una.electiva3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import py.com.una.electiva3.domain.Registro;

@Repository
public interface RegistroRepository extends CrudRepository<Registro,Long> {
	@Query("select r from Registro r where r.nroChapa = ?1 and fechaSalida = null")
	Registro findByNroChapa(String nroChapa);
	
	@Query("select r from Registro r where r.fechaSalida = null")
	List<Registro> findEntradas();
	
	@Query("select r from Registro r where fechaSalida != null")
	List<Registro> findSalidas();
}
