package py.com.una.electiva3.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import py.com.una.electiva3.domain.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario,Long>{
	Usuario findByUsername(String username);
}
