package py.com.una.electiva3;

public class JsonResult {
	private boolean successful;
	private String message;
	private Object result;
	
	public JsonResult(boolean successful, String message, Object result) {
		this.successful = successful;
		this.message = message;
		this.result = result;
	}
	
	public JsonResult(boolean successful, String message) {
		this.successful = successful;
		this.message = message;
	}
	
	public boolean isSuccessful() {
		return successful;
	}
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	
	
}
